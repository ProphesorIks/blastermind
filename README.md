# about

Blastermind is an nft game where you battle it out in a arena. The catch is: you are the weapon!

# building

For all cases:

``npm install``

``npm run start`` for development version with live reload (choose this to get instant access to the game. Then to: localhost:9000 in your browser)

``npm run build`` for development version to be built in dist/

``npm run build`` for production version to be built in dist/

# licensing

Game code is under GPL license. All items under build/assets/ are subject to copyright. When forking or building the project without explicit developers permissions those items should be replaced by items user has the rights to.